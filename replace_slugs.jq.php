.properties +=
	[{"name":"apiUrl","type":"string","value":"https://play.aaaaaaaaaaaadventu.re/pusher"}]
| (.layers[] | recurse(.layers[]?) | select(.properties != null and .properties[].name=="jitsiRoom") | .properties) +=
    [{"name":"jitsiUrl","type":"string","value":"meet.jit.si"}]
<?php
	$spacesfile = fopen("maps.csv","r");
	while (($space = fgetcsv($spacesfile,0,"\t")) !== false) {
?>| (.layers[] | recurse(.layers[]?) | .properties[]? | select(.name=="exitUrl" and (.value | startswith("world://<?php echo $space[0]?>/"))) | .value) |=
     ("/_/global/<?php echo preg_replace("/https:\/\//","",$space[2]) ?>/" + (. | ltrimstr("world://<?php echo $space[0]?>/")))
<?php
	}
	$foreignslugsfile = fopen("foreign_slugs.csv","r");
	while (($slug = fgetcsv($foreignslugsfile,0,"\t")) !== false) {
?>| (.layers[] | recurse(.layers[]?) | .properties[]? | select(.name=="exitUrl" and (.value | startswith("world://<?php echo $slug[0]?>/"))) | .value) |=
     ("/_/global/<?php echo preg_replace("/https:\/\//","",$slug[1]) ?>/" + (. | ltrimstr("world://<?php echo $slug[0]?>/")))
<?php
	}
?>
